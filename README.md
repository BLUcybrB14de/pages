<br>
<h1><em> Pages for <a href="https://cyberstation.codeberg.page">The CyberStation</a></em> <a href="https://codeberg.org/CyberStation/pages">

<img src="https://nogithub.codeberg.page/badge.svg" alt="Please don't upload to GitHub" align="right"><a href="https://codeberg.org/Coding-Liberation.net/COPL"><img src="https://www.codeberg.org/Coding-Liberation.net/COPL/raw/branch/main/images/AC-COPL-badge.webp" alt="AC-COPL-badge.webp" width="110" height="20" align="right"></a>

</h1>
<div align="right">
<h2>

> <em> You've come a long way here..  haven't you ? </em>

</h2>

</div>

<div align="left">

<h2>Hey there &mdash; Welcome to my obscure corner of the Internet ! </h2>
A place to unleash your inner peace & find your true-self. 
Feel free to stay as long or as little as you need.  

<br>
<br>
<div align="left">
</div>
</div>
<br>

<div align="left">
	<img src="https://neocities.org/site_screenshots/34/46/blucybrb14de/index.html.540x405.webp" >
</div>

<br>

<a href="https://blucybrb14de.codeberg.page/">
    <img alt="Get it on Codeberg" src="https://get-it-on.codeberg.org/get-it-on-blue-on-white.png" height="60" align="right">
</a>


<a href="http://www.w3.org/html/logo/">
<img src="https://www.w3.org/html/logo/badge/html5-badge-h-solo.png" width="32" height="32" alt="HTML5 Powered" title="HTML5 Powered">
</a>

<a href="https://1000logos.net/css-logo/">
<img src="https://www.vectorlogo.zone/logos/w3_css/w3_css-icon.svg" width="32" height="32" alt="CSS Powered" 
title="CSS Powered">
</a>

# Creating the Cybrnet - Why Codeberg Pages ?

You might be wondering what inspired me to create this page. Well, I was looking to create something new, and inspired by <a href="https://neocities.org/">neocities</a> - a super cool platform for creating personal websites that totally blew my mind. I really like the simplicity and elegance of <a href="https://en.wikipedia.org/wiki/Static_web_page">static sites</a>, especially the ones made by all you awesome <a href="https://www.gnu.org/gnu/linux-and-gnu.html">GNU/Linux</a> users out there.

<br>

<h2> I mean, who doesn't love a bit of Internet nostalgia, right? </h2>

Anyway, I decided to create my own site here on <a href="https://tonisagrista.com/blog/2022/codeberg-setup/">Codeberg Pages</a> because it's all in one repository, which makes managing and updating my content a breeze. Plus, I love having my own little spot on the web that I can customize and make my own. 

So, if you're curious about what I've created here, go ahead and explore! Who knows, you might even find something interesting and fun! So let's suffer through it together!

### Site Mirrors

<ul>
	<li> <a href="https://cyberstation.codeberg.page">On Codeberg Pages</a>
	<li> <a href="https://blucybrb14de.neocities.org">On Neocities</a>
</ul>

## Acknowledgments

- Thank you to <a href="https://codeberg.org/Codeberg-e.V.">Codeberg</a> &mdash; for providing a great platform for hosting my website without relying on big tech companies.

# Issues + Questions

- I will only respond to issues through <a href="https://codeberg.org/Codeberg/">Codeberg</a>
  - To report an issue for the configuration, please create <a href="https://docs.codeberg.org/getting-started/issue-tracking-basics">an issue</a>
- By reporting an issue, you are required to adhere to the Full <u><strong><a href="https://codeberg.org/Coding-Liberation/Manifesto/src/branch/main/CONTRIBUTING.md">CONTRIBUTING.md</a></strong></u>
    - This is a fork of the <a href="https://www.debian.org/code_of_conduct">Debian Code of Conduct</a>
      - Also note, the first clause & 3-6 of the <a href="https://www.funtoo.org/Wolf_Pack_Philosophy">Wolf-Pack philosphy</a></u> 
  - If at any point, you discover the solution to an issue, please <a href="https://www.funtoo.org/Wolf_Pack_Philosophy">Howl.</a><br>
- <strong>For Contact:</strong> <br><a href="https://proton.me/"><img src="https://codeberg.org/blucybrb14de/dotfiles/raw/branch/gentoo-hyprland/images/protonmail.webp"  style="vertical-align: middle" height="20" width="20"></a> blucybrb14de@coding-liberation.org

Mirrors

<a href="https://codeberg.org/BLUcybrB14de/pages"><img src="https://codeberg.org/Codeberg/Design/raw/branch/main/logo/icon/svg/codeberg-logo_icon_blue.svg" alt="Codeberg" width="32" /></a>
<a href="https://gitlab.com/BLUcybrB14de/pages"><img src="https://about.gitlab.com/images/press/press-kit-icon.svg" alt="Gitlab" width="32" /></a>

<hr>

<a class="tooltip" href="https://codeberg.org/Coding-Liberation.net" data-content="Coding-Liberation.net">
<img class="ui avatar gt-vm" src="https://codeberg.org/repo-avatars/be28b9fa83042b94028bf1f1b28275eebb5926f957648b5faa5842732d512e20"
title="The Coding Liberation Front - for a better future" width="28" height="28" align="left"/></a>


<strong> LICENSE  &mdash;  Unique content of this project is licensed under the <a href="https://codeberg.org/BLUcybrB14de/pages/src/branch/main/LICENSE">COPL</a> 
</strong>



